# An Introduction to R

These materials accompany the DuPRI "An Introduction to R" training. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* r_intro.R - R script version of training code
* r_intro.Rmd - Rmarkdown HTML version of training code
* r_intro.html - HTML report of training for reference
* r_intro_pdf.Rmd - Rmarkdown PDF version of training code
* r_intro_pdf.pdf - PDF report of training for reference
* Intro to R.Rproj - RStudio project file
* GSS2016.dta - 2016 GSS data set
* nba_player_stats_2017.csv - NBA player stats for 2017 season
* nba_totals_2017.csv - NBA team total stats for 2017 season
* nba_misc_2017.csv - NBA team miscellaneous stats for 2017 season
* nba_per_game_2012.csv - NBA team per game stats for 2012 season
* nba_per_game_2013.csv - NBA team per game stats for 2013 season
* nba_per_game_2014.csv - NBA team per game stats for 2014 season
* nba_per_game_2015.csv - NBA team per game stats for 2015 season
* nba_per_game_2016.csv - NBA team per game stats for 2016 season
* nba_per_game_2017.csv - NBA team per game stats for 2017 season
